'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = uuid;
function s4() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
}
/**
 * Generates a NON-RFC Complaint UUID
 * If we ever need a Complaint UUID we should use https://www.npmjs.com/package/uuid
 *
 * @returns {string} returns a NON-RFC compliant UUID
 */
function uuid() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}