'use strict';

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _sinon = require('sinon');

var _sinon2 = _interopRequireDefault(_sinon);

var _sinonChai = require('sinon-chai');

var _sinonChai2 = _interopRequireDefault(_sinonChai);

var _enzyme = require('enzyme');

var _chai = require('chai');

var _chai2 = _interopRequireDefault(_chai);

var _chaiEnzyme = require('chai-enzyme');

var _chaiEnzyme2 = _interopRequireDefault(_chaiEnzyme);

var _proxyquire = require('proxyquire');

var _proxyquire2 = _interopRequireDefault(_proxyquire);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

_chai2.default.use((0, _chaiEnzyme2.default)());
_chai2.default.use(_sinonChai2.default);

var DOCUMENT = '../facades/document';

describe('ContentBodyRest', function () {
    var mocks = void 0;
    var ContentBodyRest = void 0;

    beforeEach(function () {
        mocks = _defineProperty({}, DOCUMENT, {
            createElement: function createElement(tag) {
                return { tag: tag };
            }
        });

        ContentBodyRest = (0, _proxyquire2.default)('../content-body-rest', mocks).default;
    });

    describe('Render', function () {
        var content = void 0;

        var MockDOMNode = function MockDOMNode() {
            _classCallCheck(this, MockDOMNode);

            this.querySelectorAll = _sinon2.default.stub().returns([]);
            this.appendChild = _sinon2.default.stub();
        };

        beforeEach(function () {
            content = {
                body: '<html></html>',
                cssDependencies: '<link></link>'
            };
        });

        it('Should contain 3 divs', function () {
            var wrapper = (0, _enzyme.shallow)(_react2.default.createElement(ContentBodyRest, { content: content }));
            (0, _chai.expect)(wrapper).to.have.exactly(3).descendants('div');
        });

        it('Should have the correct content container classes', function () {
            var wrapper = (0, _enzyme.shallow)(_react2.default.createElement(ContentBodyRest, { content: content }));
            var firstContainer = wrapper.find('#content');
            (0, _chai.expect)(firstContainer).to.exist;
            var secondContainer = wrapper.find('#main-content');
            (0, _chai.expect)(secondContainer).to.exist;
            (0, _chai.expect)(firstContainer).to.have.className('page view');
            (0, _chai.expect)(secondContainer).to.have.className('wiki-content');
        });

        it('Should contain the given body and cssDependencies', function () {
            var wrapper = (0, _enzyme.shallow)(_react2.default.createElement(ContentBodyRest, { content: content }));
            var htmlDiv = wrapper.find('[dangerouslySetInnerHTML]');
            (0, _chai.expect)(htmlDiv).to.have.prop('dangerouslySetInnerHTML').deep.equal({ __html: '' + content.body + content.cssDependencies });
        });

        it('Should set correct references', function () {
            var wrapper = (0, _enzyme.shallow)(_react2.default.createElement(ContentBodyRest, { content: content }));
            var instance = wrapper.instance();
            var children = wrapper.find('#content').children();
            (0, _chai.expect)(children.at(0).node.ref).to.equal(instance._setBodyContainerRef);
            children.at(0).node.ref('body-node');
            (0, _chai.expect)(instance._bodyContainer).to.equal('body-node');

            (0, _chai.expect)(children.at(1).node.ref).to.equal(instance._setScriptContainerRef);
            children.at(1).node.ref('scripts-node');
            (0, _chai.expect)(instance._scriptContainer).to.equal('scripts-node');
        });

        describe('lifeCycleEvents', function () {
            var wrapper = void 0;
            var instance = void 0;

            beforeEach(function () {
                content.jsDependencies = ['www.google.com', 'www.someotherdomain.com'];
                wrapper = (0, _enzyme.shallow)(_react2.default.createElement(ContentBodyRest, { content: content }));

                instance = wrapper.instance();
                instance._bodyContainer = new MockDOMNode();
                instance._scriptContainer = new MockDOMNode();
            });

            describe('componentDidUpdate', function () {
                it('Should add target: "_top" to all anchors in the component', function () {
                    var nodeList = [{}, {}, {}, {}];

                    instance._bodyContainer.querySelectorAll.returns(nodeList);

                    instance.componentDidUpdate();

                    nodeList.forEach(function (node) {
                        return (0, _chai.expect)(node.target).to.equal('_top');
                    });
                });
                it('Should add script elements for each jsDependency', function () {
                    instance.componentDidUpdate();
                    content.jsDependencies.forEach(function (src) {
                        return (0, _chai.expect)(instance._scriptContainer.appendChild).to.have.been.calledWith({
                            async: true,
                            src: src,
                            tag: 'script'
                        });
                    });
                });
            });

            describe('componentDidMount', function () {
                it('Should append script elements with the jsDependencies', function () {
                    instance.componentDidMount();
                    content.jsDependencies.forEach(function (src) {
                        return (0, _chai.expect)(instance._scriptContainer.appendChild).to.have.been.calledWith({
                            async: true,
                            src: src,
                            tag: 'script'
                        });
                    });
                });

                it('Should add target: "_top" to all anchors in the component', function () {
                    var nodeList = [{}, {}, {}];

                    instance._bodyContainer.querySelectorAll.returns(nodeList);

                    instance.componentDidMount();

                    nodeList.forEach(function (node) {
                        return (0, _chai.expect)(node.target).to.equal('_top');
                    });
                });
            });
        });
    });
});