'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _contentBodyIframe = require('../content-body-iframe/content-body-iframe');

var _contentBodyIframe2 = _interopRequireDefault(_contentBodyIframe);

var _contentBodyRest = require('../content-body-rest/content-body-rest');

var _contentBodyRest2 = _interopRequireDefault(_contentBodyRest);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ContentBody = function (_Component) {
    _inherits(ContentBody, _Component);

    function ContentBody() {
        _classCallCheck(this, ContentBody);

        return _possibleConstructorReturn(this, Object.getPrototypeOf(ContentBody).apply(this, arguments));
    }

    _createClass(ContentBody, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            this._finishLoadingIfSPAFriendly(this.props);
        }
    }, {
        key: 'componentDidUpdate',
        value: function componentDidUpdate() {
            this._finishLoadingIfSPAFriendly(this.props);
        }
    }, {
        key: '_finishLoadingIfSPAFriendly',
        value: function _finishLoadingIfSPAFriendly(props) {
            var content = props.content;
            var onContentLoaded = props.onContentLoaded;

            if (content.isSPAFriendly && onContentLoaded) {
                onContentLoaded();
            }
        }
    }, {
        key: 'render',
        value: function render() {
            var _props = this.props;
            var content = _props.content;
            var baseUrl = _props.baseUrl;
            var contextPath = _props.contextPath;
            var onContentLoaded = _props.onContentLoaded;


            if (!content.isSPAFriendly) {
                return _react2.default.createElement(_contentBodyIframe2.default, { baseUrl: baseUrl, content: content, contextPath: contextPath, onContentLoaded: onContentLoaded });
            }

            return _react2.default.createElement(_contentBodyRest2.default, { content: content });
        }
    }]);

    return ContentBody;
}(_react.Component);

exports.default = ContentBody;


ContentBody.displayName = 'ContentBody';
ContentBody.propTypes = {
    /**
     * The ID of the content to render.
     */
    baseUrl: _react.PropTypes.string,
    content: _react2.default.PropTypes.shape({
        id: _react.PropTypes.string,
        body: _react2.default.PropTypes.string,
        cssDependencies: _react2.default.PropTypes.string,
        jsDependencies: _react2.default.PropTypes.array.isRequired,
        isSPAFriendly: _react2.default.PropTypes.bool.isRequired
    }),
    /**
     * Callback when content finishes loading.
     */
    onContentLoaded: _react.PropTypes.func
};