[![build-status](https://bitbucket-badges.useast.atlassian.io/badge/atlassian/confluence-react-components.svg)](https://bitbucket.org/atlassian/confluence-react-components/addon/pipelines/home) [![Coverage Status](https://coveralls.io/repos/bitbucket/atlassian/confluence-react-components/badge.svg?branch=Coverage)](https://coveralls.io/bitbucket/atlassian/confluence-react-components?branch=Coverage)

# confluence-react-components

Official Atlassian Confluence react components

## Installation

    $ npm install confluence-react-components

## Building

    $ npm run transpile

## Automatically build on file change

    $ npm run transpile-watch

## Basic usage

    // ES6
    import { ContentBody } from 'confluence-react-components';
    
    class SomeComponent {
        render() {
            return <ContentBody content={} baseUrl=''/>;
        }
    }
    
## Contributing

### Running tests

    $ npm test

### Running linter

    $ npm run lint

### Checking coverage

    $ npm run coverage

### Known issues

* Istanbul coverage checks do not work since upgrading to Babel 6.
