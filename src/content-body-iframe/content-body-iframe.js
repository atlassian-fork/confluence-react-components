import React, { Component, PropTypes } from 'react';
import uuid from '../util/uuid';
import { addEventListener, removeEventListener } from '../facades/window';

const DEFAULT_CONTEXT_PATH = '/confluence';

export default class ContentBodyIframe extends Component {
    constructor() {
        super();

        this.iframeId = uuid();

        this.state = {
            height: '1000px'
        };
    }

    componentDidMount() {
        this.receiveMessage = event => {
            const { iframeId } = event.data;

            if (!iframeId || iframeId !== this.iframeId) {
                return;
            }

            const { height } = event.data;

            this.setState({
                height: `${height}px`
            });
        };

        addEventListener('message', this.receiveMessage);
    }

    componentWillUnmount() {
        removeEventListener('message', this.receiveMessage);
    }

    render() {
        const { content, baseUrl, onContentLoaded } = this.props;
        let { contextPath } = this.props;
        const { height } = this.state;

        if (typeof contextPath === 'undefined') {
            contextPath = DEFAULT_CONTEXT_PATH;
        }

        const contentUrl = `${baseUrl}${contextPath}/content-only/viewpage.action?pageId=${content.id}&iframeId=${this.iframeId}`;

        return (
            <iframe
                src={contentUrl}
                border="0"
                style={{border:0, width: '100%', height}}
                onLoad={onContentLoaded}
            />
        );
    }
}

ContentBodyIframe.displayName = 'ContentBodyIframe';

ContentBodyIframe.defaultProps = {
    baseUrl: ''
};

ContentBodyIframe.propTypes = {
    /**
     * The ID of the content to render.
     */
    contentId: PropTypes.string,
    /**
     * Host of confluence instance for the iframe src attribute
     */
    baseUrl: PropTypes.string,
    /**
     * Confluence instance context path
     */
    contextPath: PropTypes.string,
    /**
     * Callback when iframe finishes loading.
     */
    onContentLoaded: PropTypes.func
};
