import React from 'react';

import sinon, { spy, stub } from 'sinon';
import sinonChai from 'sinon-chai';
import { shallow, render } from 'enzyme';
import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import ContentBody from '../content-body';

chai.use(chaiEnzyme());
chai.use(sinonChai);

describe('ContentBody', () => {
    let content;
    let wrapper;
    let onContentLoadedSpy;
    const baseUrl = 'www.atlassian.com';
    const contextPath = '';

    beforeEach(() => {
        onContentLoadedSpy = sinon.spy();
        content = {
            id: '123',
            body: '<html></html>',
            cssDependencies: '',
            jsDependencies: [],
            isSPAFriendly: true
        };
    });

    afterEach(() => {
        onContentLoadedSpy.reset();
    });

    describe('componentDidMount', () => {
        it('Should call onContentLoaded when it is SPA friendly', () => {
            content.isSPAFriendly = true;
            wrapper = shallow(<ContentBody content={content} baseUrl={baseUrl} onContentLoaded={onContentLoadedSpy} />);

            const instance = wrapper.instance();
            instance.componentDidMount();

            expect(onContentLoadedSpy).to.have.been.called;
        });

        it('Should not call onContentLoaded when it is not SPA friendly', () => {
            content.isSPAFriendly = false;
            wrapper = shallow(<ContentBody content={content} baseUrl={baseUrl} onContentLoaded={onContentLoadedSpy} />);

            const instance = wrapper.instance();
            instance.componentDidMount();

            expect(onContentLoadedSpy).to.not.have.been.called;
        });
    });

    describe('componentDidUpdate', () => {
        it('Should call onContentLoaded when it is SPA friendly', () => {
            content.isSPAFriendly = true;
            wrapper = shallow(<ContentBody content={content} baseUrl={baseUrl} onContentLoaded={onContentLoadedSpy} />);

            const instance = wrapper.instance();
            instance.componentDidUpdate();

            expect(onContentLoadedSpy).to.have.been.called;
        });

        it('Should not call onContentLoaded when it is not SPA friendly', () => {
            content.isSPAFriendly = false;
            wrapper = shallow(<ContentBody content={content} baseUrl={baseUrl} onContentLoaded={onContentLoadedSpy} />);

            const instance = wrapper.instance();
            instance.componentDidUpdate();

            expect(onContentLoadedSpy).to.not.have.been.called;
        });
    });

    describe('Called with content that is SPA friendly', () => {
        beforeEach(() => {
            wrapper = shallow(<ContentBody content={content} baseUrl={baseUrl} onContentLoaded={onContentLoadedSpy} />);
        });

        it('Should render ContentBodyRest', () => {
            expect(wrapper.is('ContentBodyRest')).to.equal(true);
            expect(wrapper).to.not.have.prop('onContentLoaded');
        });

        it('Should pass on the content to ContentBodyRest', () => {
            expect(wrapper).to.have.prop('content').deep.equal(content);
        });
    });

    describe('Called with content that is not SPA friendly', () => {
        beforeEach(() => {
            content.isSPAFriendly = false;
            wrapper = shallow(<ContentBody content={content} baseUrl={baseUrl} contextPath={contextPath} onContentLoaded={onContentLoadedSpy} />);
        });

        it('Should render ContentBodyIframe', () => {
            expect(wrapper.is('ContentBodyIframe')).to.equal(true);
            expect(wrapper).to.have.prop('onContentLoaded').equal(onContentLoadedSpy);
        });

        it('Should pass on the content to ContentBodyIframe', () => {
            expect(wrapper).to.have.prop('content').deep.equal(content);
        });

        it('Should pass on the baseUrl to ContentBodyIframe', () => {
            expect(wrapper).to.have.prop('baseUrl').equal(baseUrl);
        });

        it('Should pass on the contextPath to ContentBodyIframe', () => {
            expect(wrapper).to.have.prop('contextPath').equal(contextPath);
        });
    });
});
