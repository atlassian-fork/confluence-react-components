import React, { Component, PropTypes } from 'react';
import { createElement } from '../facades/document';


export default class ContentBodyRest extends Component {
    constructor() {
        super();
        this._setScriptContainerRef = this._setScriptContainerRef.bind(this);
        this._setBodyContainerRef = this._setBodyContainerRef.bind(this);
    }

    componentDidMount() {
        this._updateAnchorTargets();
        this._loadJSDependencies();
    }

    componentDidUpdate() {
        this._updateAnchorTargets();
        this._loadJSDependencies();
    }

    _updateAnchorTargets() {
        const anchors = this._bodyContainer.querySelectorAll('a');
        for (let i = 0, anchor; anchor = anchors[i]; i++) {
            anchor.target = '_top';
        }
    }

    _loadJSDependencies() {
        const content = this.props.content;
        content.jsDependencies.forEach(this._appendScriptToContainer, this);
    }

    _appendScriptToContainer(uri) {
        const scriptElement = createElement('script');
        scriptElement.async = true;
        scriptElement.src = uri;
        this._scriptContainer.appendChild(scriptElement);
    }

    _setBodyContainerRef(node) {
        this._bodyContainer = node;
    }

    _setScriptContainerRef(node) {
        this._scriptContainer = node;
    }

    render() {
        const { body, cssDependencies } = this.props.content;

        return (
            <div id="content" className="page view">
                <div dangerouslySetInnerHTML={{ __html: `${body}${cssDependencies}` }} ref={this._setBodyContainerRef} id="main-content" className="wiki-content"></div>
                <div ref={this._setScriptContainerRef}></div>
            </div>
        );
    }
}

ContentBodyRest.displayName = 'ContentBodyRest';
ContentBodyRest.propTypes = {
    /**
     * The ID of the content to render.
     */
    contentId: PropTypes.string
};
